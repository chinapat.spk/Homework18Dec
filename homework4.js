const rawData = require('./hw4raw.json')

function countEyes(data) {
    let count = { "brown": 0, "blue": 0, "green": 0 };
    for (let row in data) {
        let rowData = data[row]
        for (let key in rowData) {
            if (key == 'eyeColor') {
                if (rowData[key] == 'brown') { count.brown += 1 }
                else if (rowData[key] == 'blue') { count.blue += 1 }
                else if (rowData[key] == 'green') { count.green += 1 }
            }
        }
    }
    return count
}

function countSex(data){
    let count = { "male":0, "female":0 };
    for (let row in data) {
        let rowData = data[row]
        for (let key in rowData) {
            if (key == 'gender') {
                if (rowData[key] == 'male') { count.male += 1 }
                else if (rowData[key] == 'female') { count.female += 1 }
            }
        }
    }
    return count
}

function countFriend(data){
    for (let row in data) {
        let rowData = data[row]
        for (let key in rowData) {
            if (key == 'friends') {
                rowData.friendCount = 0
                rowData.friendCount += rowData[key].length
            }  
        }
        console.log(rowData.friendCount)
    }
    return data
}




console.log(countEyes(rawData))
console.log(countSex(rawData))
console.log(countFriend(rawData))

